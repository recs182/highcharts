// determine if app is running local or online
function isLocalhost(url = location.href){
    const regex = /192\.168\.1|localhost|127\.0\.0/i;
    const match = url.match(regex);
    if(match) return true;
    return false;
}

// requests
function Post(path, data = []){
    return fetch(`api/${path}`, {
        method: 'post',
        mode: isLocalhost() ? 'cors' : 'no-cors',
        body: JSON.stringify(data),
    })
    .then(response => {
        return response.json();
    })
    .then(success => {
        const regex = /\{.*\}|\[.*\]/;
        // if it should be an array or object but came as string it'll be parsed
        return typeof success === 'string' && success.match(regex) ? JSON.parse(success) : success;
    })
    .catch(error => error);
}

//////////////////////////////////////////
// Pollyfills
//////////////////////////////////////////
if (!Object.entries) {
    Object.entries = function entries(O) {
        return _reduce(_keys(O), (e, k) => _concat(e, typeof k === 'string' && _isEnumerable(O, k) ? [[k, O[k]]] : []), []);
    };
}