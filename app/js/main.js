const app = new Vue({
	el: '#app',
	data: {
		colors: ['#00cbff','#1d6fca','#6638a2','#ff0087','#ffcb64','#ff5100','#dddddd'],
		chart: {
			title: '',
			subtitle: '',
			start_date: '',
			end_date: '',
		}
	},
	created: function(){
		
		Post('metrics/revenue-by-medium').then(response => {
			console.log('%c response ', 'background:#4682B4;color:white;', response);
			if(!response.success || response.errors.length){
				// error message ?
				return;
			}

			// shorthand to access the response
			const metric = response.metrics.revenue_by_medium;
			
			// temp variables
			const categories    = [];
			const series_struct = {};
			metric.data.series.forEach(serie => {
				// categories
				categories.push( moment(serie.name, 'Y-M-D').format('DD/MM/Y') );

				// series
				Object.entries(serie.data).forEach(serie_arr => {
					let [name, value] = serie_arr;

					// parse values, string to number
					value = Number( value.replace(',', '.') );

					// test of key exist in the object
					series_struct.hasOwnProperty(name) ? series_struct[name].push(value) : series_struct[name] = [value];
				});
			});

			// chart title, subtitle, dates
			this.chart = {
				title: metric.title,
				subtitle: metric.description,
				start_date: moment(response.start_date, 'Y-M-D').format('DD/MM/Y'),
				end_date: moment(response.end_date, 'Y-M-D').format('DD/MM/Y'),
			};

			// chart configs
			const chart = {
				type: 'line',

				colors: this.colors,

				title: {
					margin: 0,
					text: false
				},

				xAxis: {
					categories: categories,
					labels: {
						formatter: function() {
							// display only day
							return this.value.split('/')[0];
						},
					}
				},
				
				yAxis: {
					tickInterval: 50000,
					title: {
						text: false
					}
				},

				series: Object.entries(series_struct).map((serie_arr, index) => {
					const [name, data] = serie_arr;
					return {
						name: name,
						data: data,
						// color: '#00cbff',
						marker: {
							enabled: false,
							symbol: 'circle',
						},
					}
				}),

				legend: {
					align: 'center',
					verticalAlign: 'top',
				},

				plotOptions: {
					series: {
						line: {
							linecap: 'round',
							marker: {
								enabled: false,
								symbol: 'circle',
							}
						}
					}
				},

				tooltip: {
					formatter: function() {
						const serie = this.series.name;
						const value = (Number(this.y)).toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'});
						const date  = this.x;

						return `${serie} <br /> <strong>${date}:</strong> ${value}`;
					}
				},
			};

			Highcharts.chart('highchart-container', chart);
		}, err => {
			console.log('%c err ', 'background:#4682B4;color:white;', err);
		})

	},
});