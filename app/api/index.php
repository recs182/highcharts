<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


// session_start();
require_once('__api.php');


$include = explode('/api/',$_SERVER['REQUEST_URI'] )[1].'.php'; 

if(!file_exists( $include )){
    error( "Chamada para API inválida. Verifique se a URL está correta." );
}

$php_input = file_get_contents("php://input");
$_POST     = O( fromJson( $php_input ? $php_input : "[]" ) );

switch (json_last_error()) {
    case JSON_ERROR_NONE:break;
    case JSON_ERROR_DEPTH:
        error("Erro na decodificação do JSON : 'Maximum stack depth exceeded'");
    break;
    case JSON_ERROR_STATE_MISMATCH:
        error("Erro na decodificação do JSON : 'Underflow or the modes mismatch'");
    break;
    case JSON_ERROR_CTRL_CHAR:
        error("Erro na decodificação do JSON : 'Unexpected control character found'");
    break;
    case JSON_ERROR_SYNTAX:
        error("Erro na decodificação do JSON : 'Syntax error, malformed JSON'");
    break;
    case JSON_ERROR_UTF8:
        error("Erro na decodificação do JSON : 'Malformed UTF-8 characters, possibly incorrectly encoded'");
    break;
    default:
        error("Erro na decodificação do JSON : 'Unknown error'");
    break;
}

$results = require_once($include);
if( gettype($results) == "object" ){
    $results->print();
}else{
    echo toJson( $results );
}